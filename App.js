import React,{ Component } from 'react';
import AlbumList from './src/components/AlbumList';
import LoginForm from './src/components/LoginForm';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

class App extends Component {

    render(){
        return (
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Home">
                    <Stack.Screen name="Authentication" component={LoginForm} />
                    <Stack.Screen name="Albums" component={AlbumList} />
                </Stack.Navigator>
            </NavigationContainer>
            
        )
    }
}

export default App;
