import * as firebase from "firebase";

const config = {
    apiKey: "AIzaSyDUjSki1ML4PWtyorTSRsPPiEb0U-_rJHU",
    authDomain: "authentication-7e69c.firebaseapp.com",
    databaseURL: "https://authentication-7e69c.firebaseio.com",
// projectId: "authentication-7e69c",
    storageBucket: "authentication-7e69c.appspot.com",
    messagingSenderId: "217029682709",
// appId: "1:217029682709:web:b40b1203964548d4e9101c",
// measurementId: "G-LDN0ZB2RTB"

};

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();