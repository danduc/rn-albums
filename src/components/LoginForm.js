import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Button, Card, CardSection, Input, Spinner } from './common';
import firebase from '../Firebase';

class LoginForm extends Component {

    state = { email:'',password:'',error:'', loading:false }

    onButtonPressed() {

        const { email, password } = this.state;

        this.setState({ error:'',loading:true });

        firebase.auth().signInWithEmailAndPassword(email,password)
               .then(this.onLoginSuccess.bind(this))
               .catch(() => {
                   firebase.auth().createUserWithEmailAndPassword(email,password)
                           .then(this.onLoginSuccess.bind(this))
                           .catch(this.onLoginFail.bind(this));
               })
    }

    onLoginFail() {
       this.setState({
           error:'Authentication Failed.',
           loading:false
       })
    }

    onLoginSuccess() {
        this.setState( { 
            email:'',
            password:'',
            loading:false,
            error:''
        });

        this.props.navigation.navigate('Albums');
    }

    renderButton() {
        if(this.state.loading){
            return <Spinner size="small" />
        }else{
            return <Button onPress={ this.onButtonPressed.bind(this) }>Log in</Button>
        }
    }

    render(){
        return (
            <View>   
                <Card>
                    <CardSection>
                        <Input label="Email"
                                placeholder="user@gmail.com"
                                value={ this.state.email }
                                onChangeText={ email => this.setState({ email })}/>
                    </CardSection>
                    <CardSection>
                        <Input label="Password"
                               placeholder="password"
                               secureTextEntry
                               value={ this.state.password }
                               onChangeText={ password => this.setState({ password })} />
                    </CardSection>
                    <Text style={ styles.errorTextStyle }>{ this.state.error }</Text>
                    <CardSection>
                        { this.renderButton() }
                    </CardSection>
                </Card>
            </View>
        );
    }
}

const styles = {
    errorTextStyle : {
        fontSize:20,
        alignSelf:'center',
        color:'red'
    }
}

export default LoginForm;